
## 0.3.4 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-db_oracle!12

---

## 0.3.3 [08-14-2024]

* Changes made at 2024.08.14_19:29PM

See merge request itentialopensource/adapters/adapter-db_oracle!11

---

## 0.3.2 [08-07-2024]

* Changes made at 2024.08.07_10:52AM

See merge request itentialopensource/adapters/adapter-db_oracle!10

---

## 0.3.1 [07-26-2024]

* manual migration updates

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!8

---

## 0.3.0 [07-09-2024]

* Add routes

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!7

---

## 0.2.1 [05-15-2024]

* Patch/adapt 3377

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!6

---

## 0.2.0 [01-06-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!3

---

## 0.1.8 [06-22-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!2

---

## 0.1.7 [06-22-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!2

---

## 0.1.6 [11-21-2019]

* Bug fixes and performance improvements

See commit 1253be6

---

## 0.1.5 [11-21-2019]

* Bug fixes and performance improvements

See commit 0f588d3

---

## 0.1.4 [11-21-2019]

* Bug fixes and performance improvements

See commit ca4d362

---
