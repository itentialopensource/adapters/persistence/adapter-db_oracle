# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Oracle System. The API that was used to build the adapter for Oracle is usually available in the report directory of this adapter. The adapter utilizes the Oracle API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Oracle Database adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Oracle Database. With this adapter you have the ability to perform operations with Oracle Database on items such as:

- Storage of Information
- Retrieval of Information

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
