/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global log */
/* eslint no-underscore-dangle: warn  */
/* eslint no-loop-func: warn */
/* eslint no-cond-assign: warn */
/* eslint no-unused-vars: warn */
/* eslint consistent-return: warn */

/* Required libraries.  */
const fs = require('fs-extra');
const path = require('path');

// libraries to import
const oracle = require('oracledb');
// Itential framework event
const EventEmitter = require('events');

let myid = null;
let errors = [];

/**
 * @summary Build a standard error object from the data provided
 *
 * @function formatErrorObject
 * @param {String} origin - the originator of the error (optional).
 * @param {String} type - the internal error type (optional).
 * @param {String} variables - the variables to put into the error message (optional).
 * @param {Integer} sysCode - the error code from the other system (optional).
 * @param {Object} sysRes - the raw response from the other system (optional).
 * @param {Exception} stack - any available stack trace from the issue (optional).
 *
 * @return {Object} - the error object, null if missing pertinent information
 */
function formatErrorObject(origin, type, variables, sysCode, sysRes, stack) {
  log.trace(`${myid}-adapter-formatErrorObject`);

  // add the required fields
  const errorObject = {
    icode: 'AD.999',
    IAPerror: {
      origin: `${myid}-unidentified`,
      displayString: 'error not provided',
      recommendation: 'report this issue to the adapter team!'
    }
  };

  if (origin) {
    errorObject.IAPerror.origin = origin;
  }
  if (type) {
    errorObject.IAPerror.displayString = type;
  }

  // add the messages from the error.json
  for (let e = 0; e < errors.length; e += 1) {
    if (errors[e].key === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    } else if (errors[e].icode === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    }
  }

  // replace the variables
  let varCnt = 0;
  while (errorObject.IAPerror.displayString.indexOf('$VARIABLE$') >= 0) {
    let curVar = '';

    // get the current variable
    if (variables && Array.isArray(variables) && variables.length >= varCnt + 1) {
      curVar = variables[varCnt];
    }
    varCnt += 1;
    errorObject.IAPerror.displayString = errorObject.IAPerror.displayString.replace('$VARIABLE$', curVar);
  }

  // add all of the optional fields
  if (sysCode) {
    errorObject.IAPerror.code = sysCode;
  }
  if (sysRes) {
    errorObject.IAPerror.raw_response = sysRes;
  }
  if (stack) {
    errorObject.IAPerror.stack = stack;
  }

  // return the object
  return errorObject;
}

/**
 * @summary Check if connection error
 *
 * @function isConnectionError
 * @param {object} error - error from Oracle
 *
 * @return {boolean} - the result of error type checking
 */

function isConnectionError(error) {
  return error && (
    error.code === 'NJS-003'
    || error.code === 'NJS-500'
    || error.code === 'NJS-040'
    || error.code === 'NJS-521'
    || error.message.includes('connect')
  );
}

class Oracle extends EventEmitter {
  constructor(prongid, properties) {
    log.trace('adapter oracle loading');
    // Instantiate the EventEmitter super class
    super();

    this.props = properties;
    this.alive = false;
    this.id = prongid;
    myid = prongid;

    // get the path for the specific error file
    const errorFile = path.join(__dirname, '/error.json');

    // if the file does not exist - error
    if (!fs.existsSync(errorFile)) {
      const origin = `${this.id}-adapter-constructor`;
      log.warn(`${origin}: Could not locate ${errorFile} - errors will be missing details`);
    }

    // Read the action from the file system
    const errorData = JSON.parse(fs.readFileSync(errorFile, 'utf-8'));
    ({ errors } = errorData);
  }

  /**
   * Itential connect call for system green/red light
   *
   * @param {function} callback
   */
  connect() {
    const meth = 'adapter-connect';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    // Set the connection parameters
    const connAttrs = {
      user: this.props.authentication.username,
      password: this.props.authentication.password,
      connectString: `${this.props.host}:${this.props.port}/${this.props.database}`
    };

    this.connAttrs = connAttrs;

    if (this.props.databaseConnection === 'connect on request') {
      return this.emit('ONLINE', {
        id: this.id
      });
    }

    log.debug('Connecting to Oracle Database with provided auth and connectString');

    // Get connection
    oracle.getConnection(connAttrs, (error, gotConn) => {
      // Emit online if on stub mode
      if (this.props && this.props.stub === true) {
        this.emit('ONLINE', {
          id: this.id
        });
        log.info('EMITTED ONLINE ON STUB MODE');
      } else if (error) {
        log.error(`error connecting to Oracle: ${error}`);
        // emit failure
        this.alive = false;
        this.emit('OFFLINE', {
          id: this.id
        });
      } else {
        // Emit success
        this.alive = true;
        this.emit('ONLINE', {
          id: this.id
        });
        this.connection = gotConn;
        log.info('Connected to Oracle.'); // Removed the specific connection details
      }
    });
  }

  /**
   * Call to run a healthcheck on the oracle database
   *
   * @function healthCheck
   * @param {healthCallback} callback - a callback function to return a result
   *                                    healthcheck success or failure
   */
  healthCheck(callback) {
    const meth = 'adapter-healthCheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify that we are connected to Oracle
      if (!this.alive || !this.connection) {
        log.error('Error during healthcheck: Not connected to Oracle Database');
        return callback({
          id: this.id,
          status: 'fail'
        });
      }

      this.connection.query('SELECT "test";', (error, results) => {
        if (error) {
          log.error(`Error during healthcheck: ${error}`);
          return callback({
            id: this.id,
            status: 'fail'
          });
        }

        log.info(`result from oracle connect: ${JSON.stringify(results)}`);
        return callback({
          id: this.id,
          status: 'success'
        });
      });
    } catch (ex) {
      log.error(`Exception during healthcheck: ${ex}`);
      return callback({
        id: this.id,
        status: 'fail'
      });
    }
  }

  /**
   * getAllFunctions is used to get all of the exposed function in the adapter
   *
   * @function getAllFunctions
   */
  getAllFunctions() {
    let myfunctions = [];
    let obj = this;

    // find the functions in this class
    do {
      const l = Object.getOwnPropertyNames(obj)
        .concat(Object.getOwnPropertySymbols(obj).map((s) => s.toString()))
        .sort()
        .filter((p, i, arr) => typeof obj[p] === 'function' && p !== 'constructor' && (i === 0 || p !== arr[i - 1]) && myfunctions.indexOf(p) === -1);
      myfunctions = myfunctions.concat(l);
    }
    while (
      (obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj)
    );

    return myfunctions;
  }

  /**
   * getWorkflowFunctions is used to get all of the workflow function in the adapter
   *
   * @function getWorkflowFunctions
   */
  getWorkflowFunctions() {
    const myfunctions = this.getAllFunctions();
    const wffunctions = [];

    // remove the functions that should not be in a Workflow
    for (let m = 0; m < myfunctions.length; m += 1) {
      if (myfunctions[m] === 'addListener') {
        // got to the second tier (adapterBase)
        break;
      }
      if (myfunctions[m] !== 'connect' && myfunctions[m] !== 'healthCheck'
        && myfunctions[m] !== 'getAllFunctions' && myfunctions[m] !== 'getWorkflowFunctions') {
        wffunctions.push(myfunctions[m]);
      }
    }

    return wffunctions;
  }

  /**
   * Call to query the Oracle server.
   * @function query
   * @param sql - a sql string (required)
   * @param {Object} options - options to provide with the query
   * @param callback - a callback function to return a result
   */
  query(sql, callback) {
    const meth = 'adapter-query';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    let oracleConnection;
    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const executeQuery = (connection, retry = false) => {
        connection.execute(sql, async (exErr, results) => {
          try {
            if (connection && this.props.databaseConnection === 'connect on request') {
              this.alive = false;
              await connection.close();
            }

            if (exErr) {
              // If connect on startup and get connection error, attempt to reconnect to Oracle Database and execute the query again
              if (retry && isConnectionError(exErr)) {
                log.warn(`${origin}: Connection error detected, attempting to reconnect to Oracle Database...`);
                oracle.getConnection(this.connAttrs, (connErr, gotConn) => {
                  if (connErr) {
                    const errorObj = formatErrorObject(origin, 'Oracle Database connection failed', null, null, null, null);
                    log.error(`${origin}: Error connecting to Oracle - ${connErr}`);
                    return callback(null, errorObj);
                  }
                  this.connection = gotConn;
                  // Only retry once so retry flag is set to false
                  executeQuery(this.connection);
                });
              } else {
                // If connect on request or connect on startup with nonconnection error, just return the error
                const errorObj = formatErrorObject(origin, 'Database Error', [exErr], null, null, null);
                log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
                return callback(null, errorObj);
              }
            } else {
              log.debug(`result from query: ${JSON.stringify(results)}`);
              return callback({
                status: 'success',
                code: 200,
                response: results
              });
            }
          } catch (e) {
            const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, e);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
        });
      };

      if (this.props.databaseConnection === 'connect on request') {
        log.debug(`${origin}: Connecting to Oracle Database`);

        oracle.getConnection(this.connAttrs, (error, gotConn) => {
          if (error) {
            const errorObj = formatErrorObject(origin, 'Oracle Database connection failed', null, null, null, null);
            log.error(`${origin}: Error connecting to Oracle - ${error}`);
            return callback(null, errorObj);
          }
          this.alive = true;
          oracleConnection = gotConn;
          executeQuery(oracleConnection);
        });
      } else {
        executeQuery(this.connection, true);
      }
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to create a table into Oracle server.
   * @function create
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  create(sql, callback) {
    const meth = 'adapter-create';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('create')) {
        const errorObj = formatErrorObject(origin, 'SQL statement must start with "CREATE"', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to select item from Oracle server.
   * @function select
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  select(sql, callback) {
    const meth = 'adapter-select';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('select')) {
        const errorObj = formatErrorObject(origin, 'SQL statement must start with "SELECT"', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to insert item into Oracle server.
   * @function insert
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  insert(sql, callback) {
    const meth = 'adapter-insert';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('insert')) {
        const errorObj = formatErrorObject(origin, 'SQL statement must start with "INSERT"', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to update item into Oracle server.
   * @function update
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  update(sql, callback) {
    const meth = 'adapter-update';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('update')) {
        const errorObj = formatErrorObject(origin, 'SQL statement must start with "UPDATE"', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to delete item from Oracle server.
   * @function delete
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  delete(sql, callback) {
    const meth = 'adapter-delete';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('delete')) {
        const errorObj = formatErrorObject(origin, 'SQL statement must start with "DELETE"', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to drop table from Oracle server.
   * @function drop
   * @param sql - a sql string (required)
   * @param callback - a callback function to return a result
   */
  drop(sql, callback) {
    const meth = 'adapter-drop';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    log.trace(`oracle drop started with sql: ${sql}`);
    try {
      // verify the required data has been provided
      if (!sql) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['sql'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!sql.toLowerCase().startsWith('drop')) {
        return callback(null, 'SQL statement must start with "DROP"');
      }

      this.query(sql, callback);
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

// export to Itential
module.exports = Oracle;
