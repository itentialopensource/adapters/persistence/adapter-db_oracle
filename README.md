Oracle Adapter
===========

This adapter allows interaction with a Oracle server. For efficiency, this adapter should only be used in IAP workflow calls. Calling the adapter from Applications instead of using the npm oracledb pacakges will be less efficient!

License & Maintainers
---

### Maintained by:

Itential Adapter Team (<product_team@itential.com>)

Check the [changelog](CHANGELOG.md) for the latest changes.

### License

Itential, LLC proprietary

### Resource to install Oracle Client

https://www.oracle.com/database/technologies/instant-client.html

### Troubleshoot

Error: [\"error connecting to Oracle: Error: DPI-1047: Cannot locate a 64-bit Oracle Client library: \\\"libclntsh.so: cannot open shared object file: No such file or directory\\\".

This error tells the adapter is not able to find lib directory. Verify the following steps from documentation[https://oracle.github.io/node-oracledb/INSTALL.html]:

3.2.3.4 Install the free Oracle Instant Client ‘Basic’ RPM
Download the latest version of the free Basic RPM from yum.oracle.com.

Instant Client is available for Oracle Linux 7 and Oracle Linux 8. Older Oracle Instant Clients are also available in the Oracle Linux 6, Oracle Linux 7 and Oracle Linux 8 repositories. The RPMs are also available from Oracle Technology Network.

Install Instant Client Basic with sudo or as the root user. You can install directly from yum.oracle.com, for example using:

sudo yum -y install oracle-release-el7
sudo yum-config-manager --enable ol7_oracle_instantclient
sudo yum -y install oracle-instantclient19.11-basic
Alternatively you can manually download the RPM and install from your local file system:

sudo yum install oracle-instantclient19.11-basic-19.11.0.0.0-1.x86_64.rpm
The link instantclient-basic-linuxx64.zip will download the latest version available from OTN.

If you have a ULN subscription, another alternative is to use yum to install the Basic package after enabling the ol7_x86_64_instantclient or ol6_x86_64_instantclient repository, depending on your version of Linux.

Using any of these methods will install the required libaio package, if it is not already present. When using Instant Client 19 on recent Linux versions, such as Oracle Linux 8, you may also need to manually install the libnsl package. This is not needed from Instant Client 21 onward.

For Instant Client 19 RPMs, the system library search path is automatically configured during installation. For older versions, if there is no other Oracle software on the machine that will be impacted, then permanently add Instant Client to the run-time link path. For example, with sudo or as the root user:

sudo sh -c "echo /usr/lib/oracle/18.3/client64/lib > /etc/ld.so.conf.d/oracle-instantclient.conf"
sudo ldconfig
Alternatively, for version 18 and earlier, every shell running Node.js will need to have the link path set:

export LD_LIBRARY_PATH=/usr/lib/oracle/18.3/client64/lib
