# Oracle Database SQL

Vendor: Oracle
Homepage: https://www.Oracle.com/

Product: Oracle Database
Product Page: https://www.Oracle.com/database/

## Introduction
We classify Oracle Database into the Data Storage domaina as Oracle Database is a database which provides the storage of information. 

"Deploy on premises and on an infrastructure as a service (IaaS)"
"Deliver high performance and availability to process mission-critical transactions"
"Provide a proven, flexible SQL database system to reduce risk and increase agility"
"Lower operational costs with a resource-efficient relational database server"

## Why Integrate
The Oracle Database adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Oracle Database. With this adapter you have the ability to perform operations with Oracle Database on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [Oracle Database SQL Reference](https://www.oracle.com/database/technologies/appdev/sql.html)
The [Oracle Database Node Library Documentation](https://www.npmjs.com/package/oracle)