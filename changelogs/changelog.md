
## 0.1.8 [06-22-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!2

---

## 0.1.7 [06-22-2023]

* Remove any sensitive data logging

See merge request itentialopensource/adapters/persistence/adapter-db_oracle!2

---

## 0.1.6 [11-21-2019]

* Bug fixes and performance improvements

See commit 1253be6

---

## 0.1.5 [11-21-2019]

* Bug fixes and performance improvements

See commit 0f588d3

---

## 0.1.4 [11-21-2019]

* Bug fixes and performance improvements

See commit ca4d362

---
